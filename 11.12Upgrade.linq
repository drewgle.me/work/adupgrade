<Query Kind="Program">
  <Reference Relative="BouncyCastle.Crypto.dll">.\BouncyCastle.Crypto.dll</Reference>
  <Reference Relative="Common.Logging.Core.dll">.\Common.Logging.Core.dll</Reference>
  <Reference Relative="Common.Logging.dll">.\Common.Logging.dll</Reference>
  <Reference Relative="Dapper.Contrib.dll">.\Dapper.Contrib.dll</Reference>
  <Reference Relative="Dapper.StrongName.dll">.\Dapper.StrongName.dll</Reference>
  <Reference Relative="EntityFramework.dll">.\EntityFramework.dll</Reference>
  <Reference Relative="EntityFramework.SqlServer.dll">.\EntityFramework.SqlServer.dll</Reference>
  <Reference Relative="Issuetrak.Domain.Common.dll">.\Issuetrak.Domain.Common.dll</Reference>
  <Reference Relative="Issuetrak.Domain.Common.Security.Encryption.dll">.\Issuetrak.Domain.Common.Security.Encryption.dll</Reference>
  <Reference Relative="Issuetrak.Domain.DataAccess.dll">.\Issuetrak.Domain.DataAccess.dll</Reference>
  <Reference Relative="Issuetrak.Domain.Encoding.dll">.\Issuetrak.Domain.Encoding.dll</Reference>
  <Reference Relative="Issuetrak.Domain.LicenseKey.Reader.dll">.\Issuetrak.Domain.LicenseKey.Reader.dll</Reference>
  <Reference Relative="Issuetrak.Domain.Licensing.Common.dll">.\Issuetrak.Domain.Licensing.Common.dll</Reference>
  <Reference Relative="Issuetrak.Domain.Licensing.dll">.\Issuetrak.Domain.Licensing.dll</Reference>
  <Reference Relative="Issuetrak.Domain.Models.dll">.\Issuetrak.Domain.Models.dll</Reference>
  <Reference Relative="Issuetrak.Domain.SecureStorage.dll">.\Issuetrak.Domain.SecureStorage.dll</Reference>
  <Reference Relative="Issuetrak.Middleware.ActiveDirectory.dll">.\Issuetrak.Middleware.ActiveDirectory.dll</Reference>
  <Reference Relative="Issuetrak.Services.Email.Incoming.ActiveDirectoryWrapper.dll">.\Issuetrak.Services.Email.Incoming.ActiveDirectoryWrapper.dll</Reference>
  <Reference Relative="Issuetrak.Services.Email.Incoming.Common.dll">.\Issuetrak.Services.Email.Incoming.Common.dll</Reference>
  <Reference Relative="Issuetrak.Services.Email.Incoming.MessageProcessor.dll">.\Issuetrak.Services.Email.Incoming.MessageProcessor.dll</Reference>
  <Reference Relative="Issuetrak.Services.Email.Incoming.NLogAdapter.dll">.\Issuetrak.Services.Email.Incoming.NLogAdapter.dll</Reference>
  <Reference Relative="Issuetrak.Services.Email.Incoming.SecureStorageWrapper.dll">.\Issuetrak.Services.Email.Incoming.SecureStorageWrapper.dll</Reference>
  <Reference Relative="Issuetrak.Services.Email.Incoming.Services.dll">.\Issuetrak.Services.Email.Incoming.Services.dll</Reference>
  <Reference Relative="MessageTemplates.dll">.\MessageTemplates.dll</Reference>
  <Reference Relative="Microsoft.Extensions.DependencyInjection.Abstractions.dll">.\Microsoft.Extensions.DependencyInjection.Abstractions.dll</Reference>
  <Reference Relative="Microsoft.Extensions.DependencyInjection.dll">.\Microsoft.Extensions.DependencyInjection.dll</Reference>
  <Reference Relative="Microsoft.Win32.Primitives.dll">.\Microsoft.Win32.Primitives.dll</Reference>
  <Reference Relative="Newtonsoft.Json.dll">.\Newtonsoft.Json.dll</Reference>
  <Reference Relative="NLog.dll">.\NLog.dll</Reference>
  <Reference Relative="System.ValueTuple.dll">.\System.ValueTuple.dll</Reference>
  <Reference Relative="SecurityDriven.Inferno.dll">.\SecurityDriven.Inferno.dll</Reference>
  <Reference>&lt;RuntimeDirectory&gt;\System.IO.Compression.dll</Reference>
  <Reference>&lt;RuntimeDirectory&gt;\System.IO.Compression.ZipFile.dll</Reference>
  <Namespace>Common.Logging</Namespace>
  <Namespace>Dapper</Namespace>
  <Namespace>Issuetrak.Domain.Common.ExtensionMethods</Namespace>
  <Namespace>Issuetrak.Domain.Common.Interactors</Namespace>
  <Namespace>Issuetrak.Domain.Common.Interfaces</Namespace>
  <Namespace>Issuetrak.Domain.DataAccess</Namespace>
  <Namespace>Issuetrak.Domain.DataAccess.Infrastructure</Namespace>
  <Namespace>Issuetrak.Domain.DataAccess.Infrastructure.Interfaces</Namespace>
  <Namespace>Issuetrak.Domain.DataAccess.Logging</Namespace>
  <Namespace>Issuetrak.Domain.DataAccess.Repository</Namespace>
  <Namespace>Issuetrak.Domain.SecureStorage.Interactors</Namespace>
  <Namespace>Issuetrak.Domain.SecureStorage.Interfaces</Namespace>
  <Namespace>Issuetrak.Middleware.ActiveDirectory.Infrastructure.Dispatchers</Namespace>
  <Namespace>Issuetrak.Middleware.ActiveDirectory.Infrastructure.Factories</Namespace>
  <Namespace>Issuetrak.Middleware.ActiveDirectory.Infrastructure.Models</Namespace>
  <Namespace>Issuetrak.Services.Email.Incoming.ActiveDirectoryWrapper</Namespace>
  <Namespace>Issuetrak.Services.Email.Incoming.Common</Namespace>
  <Namespace>Issuetrak.Services.Email.Incoming.Common.Configuration</Namespace>
  <Namespace>Issuetrak.Services.Email.Incoming.Common.Logger</Namespace>
  <Namespace>Issuetrak.Services.Email.Incoming.EmailRetrieverService</Namespace>
  <Namespace>Issuetrak.Services.Email.Incoming.NLogAdapter</Namespace>
  <Namespace>Issuetrak.Services.Email.Incoming.SecureStorageWrapper</Namespace>
  <Namespace>Issuetrak.Services.Email.Incoming.Services</Namespace>
  <Namespace>Microsoft.Extensions.DependencyInjection</Namespace>
  <Namespace>System.Data.Entity.Infrastructure.Interception</Namespace>
  <Namespace>System.Threading.Tasks</Namespace>
  <Namespace>System.Xml.Serialization</Namespace>
</Query>

async Task Main()
{
	var sites = new EmailConfigurationReader().GetSites(File.OpenText(@"C:\Program Files (x86)\Issuetrak\Common\Configuration\Services\EmailConfiguration.xml"));
	
	var tasks = new List<Task>();
	foreach(var site in sites){
		tasks.Add(UpgradeSite(site));
	}
	await Task.WhenAll(tasks);
}

async Task UpgradeSite(string sitePath)
{
	try
	{
		var provider = DIBuilder.BuildDI(sitePath);
		var database = provider.GetRequiredService<IDbConnection>();
		await UpdateSchema(database);
		var adServers = (await database
			.QueryAsync<ActiveDirectoryConnectionInfo>("Select * From ADParameters")).ToList();
		var messages = new List<string>();
		messages.Add($"Found {adServers.Count} Servers");
		foreach (var adServer in adServers)
		{
			messages.Add(await UpdateAdEntry(database, provider, adServer));
		}
		messages.Dump(sitePath);
	}
	catch (Exception e)
	{
		$"Something went wrong with site. {e.Message}".Dump(sitePath);
	}
}

public async Task UpdateSchema(IDbConnection database)
{
	await database.ExecuteAsync(@"
	IF NOT EXISTS(SELECT Count(*) FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_NAME = 'ADParameters' AND COLUMN_NAME = 'UserId') 
	BEGIN
		ALTER TABLE[ADParameters] ADD[UserId] VARCHAR(256) NULL
	END");
}

public async Task<string> UpdateAdEntry(IDbConnection database, ServiceProvider provider, ActiveDirectoryConnectionInfo adServer)
{
	try
	{
		var dependencyResolver = new IEMDependencyResolver(provider);
		var ldapConnection = await LdapServerFactory.FromServerIdAsync(dependencyResolver, adServer.ADParametersID);
		var user = ldapConnection.SearchForSingleEntityProperty($"(&(objectclass=user)(objectcategory=person)(distinguishedName={adServer.AdminID}))", AccessedActiveDirectoryProperties.Text.SamAccountName);
		await database.ExecuteAsync("Update ADParameters SET UserID = @UserId WHERE ADParametersId = @ServerId", new { UserId = $"{adServer.Domain}\\{user}", ServerId = adServer.ADParametersID });
		return $"server {adServer.ADParametersID}: {adServer.Server} updated with {adServer.Domain}\\{user}";
	}
	catch(Exception ex)
	{
		return $"Could not connect to server {adServer.ADParametersID}: {adServer.Server} {adServer.Domain}";
	}
}

public static class DIBuilder
{

	public static ServiceProvider BuildDI(string sitePath)
	{
		var webConfigPath = Path.Combine(sitePath, "core", "web.config");
		var serviceCollection = new ServiceCollection()
			.AddSingleton(Issuetrak.Services.Email.Incoming.Common.Configuration.ServiceConfiguration.BuildFromArguments(new[] { "-disableCertificateRevocationCheck", webConfigPath }))
			.AddSingleton<Issuetrak.Services.Email.Incoming.Common.Configuration.IConfigurationReader, ConfigurationReader>()
			.AddSingleton<IGlobalServicesConfigurationReader, GlobalServicesConfigurationReader>()
			.AddSingleton(sp => sp.GetRequiredService<Issuetrak.Services.Email.Incoming.Common.Configuration.IConfigurationReader>().GetSiteConfiguration())
			.AddTransient<IDbConnection>(sp => new SqlConnection(sp.GetRequiredService<SiteConfiguration>().ConnectionString))
			.AddSingleton(typeof(ILogger<>), typeof(NLogLogger<>))
			.AddTransient(typeof(IRepositoryAsync<>), typeof(Repository<>))
			.AddTransient<IUnitOfWorkAsync, UnitOfWork>()
			.AddTransient<IDataContextAsync>(sp => new Issuetrak.Domain.DataAccess.Repository.DataContext(sp.GetRequiredService<IDbConnection>().ConnectionString))
			.AddTransient<IDbCommandInterceptor>(sp => new LoggingDbCommandInterceptor(sp.GetRequiredService<ILog>()))
			.AddTransient<ILog, LogWrapper>()
			.AddTransient<ITracingInteractor, IEMTracingInteractor>()
			.AddSingleton<ISecurePasswordStorage, SecureDataWrapper>()
			.AddSingleton<ISecureStorage>(sp => new SecureData(
				sp.GetRequiredService<ISecureKeyStorage>(),
				sp.GetRequiredService<IProtectedDataRepository>(),
				sp.GetRequiredService<ServiceConfiguration>().PathToConfigurationXML
				))
			.AddSingleton<ISecureKeyStorage, SecureKeyStorage>()
			.AddSingleton<IProtectedDataRepository, DapperProtectedDataRepository>()
			;

		return serviceCollection.BuildServiceProvider();
	}
}
public class EmailConfigurationReader
{

	public IList<string> GetSites(TextReader xml)
	{
		try
		{
			var xmlReader = new XmlSerializer(typeof(EmailConfiguration));
			var parsed = (EmailConfiguration)xmlReader.Deserialize(xml);
			var activeSites = parsed.Sites.Select(s => s.WebRootPath).ToList();
			activeSites.Dump($"found {activeSites.Count} active sites");
			return activeSites;
		}
		catch (InvalidOperationException ex)
		{
			ex.Dump();
			return new List<string>();
		}
	}
	public class EmailConfiguration
	{
		[XmlArray("Sites")]
		public List<Site> Sites { get; set; } = new List<Site>();
	}
	
	public class Site
	{
		[XmlAttribute]
		public string WebRootPath { get; set; }
	}
}